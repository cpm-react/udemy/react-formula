import React from "react";
import { Formik, useField } from "formik";
import * as yup from "yup";
import { max } from "lodash";

const Campo = ({ label, ...props }) => {
  const [field, meta] = useField(props);
  console.log(field);
  console.log(meta);

  return (
    <div className="form-group">
      <label htmlFor={props.id}>{label}</label>
      <input
        {...field}
        {...props}
        className={meta.error && meta.touched ? "is-invalid" : ""}
      />
      {meta.error && meta.touched ? (
        <div className="invalid-feedback">{meta.error}</div>
      ) : (
        ""
      )}
    </div>
  );
};

const AdicionaCliente = () => {
  const esquema = yup.object({
    nome: yup
      .string()
      .required("Insira o nome")
      .min(10, "O nome deve ter no minimo 10 caracteres")
      .max(30, "O nome deve ter no máximo 30 caracteres"),
    email: yup.string().required("Insira o email").email("O email é inválido"),
    nascimento: yup
      .date()
      .required("Insira a data nascimento")
      .max(new Date(), "Tá muito jovem, safado"),
  });

  return (
    <>
      <h1>Cadastro de Clientes</h1>

      <Formik
        initialValues={{ nome: "", email: "", nascimento: "" }}
        validationSchema={esquema}
        onSubmit={(values) => {
          alert(JSON.stringify(values));
        }}
      >
        {(props) => (
          <form noValidate onSubmit={props.handleSubmit}>
            <Campo id="nome" name="nome" type="text" label="Nome" />
            <Campo id="email" name="email" type="email" label="Email" />
            <Campo
              id="nascimento"
              name="nascimento"
              type="date"
              label="Data de Nascimento"
            />
            <button type="submit">Adicionar</button>
          </form>
        )}
      </Formik>
    </>
  );
};

export default AdicionaCliente;
